package droneconsole;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class DroneInterface {

	private Scanner s;								// scanner used for input from user
	public  DroneArena myArena;						// arena in which drones are shown

	public DroneInterface() {
		s = new Scanner(System.in);			// set up scanner for user input
		myArena = new DroneArena(20, 6);	// create arena of size 20*6

		char ch = ' ';
		do {
			System.out.print("Enter (A)dd drone, get (I)nformation, (D)isplay, (M)ove or e(X)it > ");
			ch = s.next().charAt(0);
			s.nextLine();
			switch (ch) {
			case 'A' :
			case 'a' :
				myArena.addDrone();				// add a new drone to arena
				break;
			case 'D' :
			case 'd' : 
				this.doDisplay();				//Display the arena including drones
				break;	
			case 'I' :
			case 'i' :
				System.out.print(myArena.toString());	//Shows info on drones and arena
				break;
			case 'M':
			case 'm':
				for(int ct = 0; ct < 10; ct++) {
					myArena.moveArena();
					this.doDisplay();
					try {
						TimeUnit.MILLISECONDS.sleep(200);			//moves each drone and prints their position and display
					} 
					catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				break;
			case 'x' : 	ch = 'X';				// when X detected program ends
			break;
			}
		}
		while (ch != 'X');						// test if end

		s.close();									// close scanner    
	}

	void doDisplay() {
		ConsoleCanvas c = new ConsoleCanvas(myArena.getX()+ 2, myArena.getY() + 2);  //sets bound of the canvas to encapsulate arena
		myArena.showDrones(c);
		System.out.println(c.toString());
	}	

	public static void main(String[] args) {
		DroneInterface r = new DroneInterface();	// just call the interface
	}

}
