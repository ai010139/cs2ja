package droneconsole;

import java.util.Random;

public enum Direction {
	North,
    East,
    South,
    West;
   
    public static Direction getRandomDirection() {	
        Random random = new Random();	
        return values()[random.nextInt(values().length)];			//picks a random enum
    }
     
	 public Direction next() {
        	
		 return values()[(ordinal() + 1) % values().length];		// gives the next enum clockwise, loops from west to north.
		
	 }    
}