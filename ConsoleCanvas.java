package droneconsole;

import java.util.Arrays;

public class ConsoleCanvas {
	
	private int xcan, ycan;
	char [][] canvas;
	
	public ConsoleCanvas (int cx, int cy) {		
		
		xcan = cx;
		ycan = cy;
		canvas = new char[ycan][xcan]; 							//creates a 2d Array for each x and y, REMEBER THAT TOP LEFT IS 0, 0 HENCE NORTH AND SOUTH ARE FLIPPED
		for (char[] row: canvas)
            Arrays.fill(row, ' ');
		
		for (int y = 0; y < canvas.length; y++) {
			
			for (int x = 0; x < canvas[0].length; x++) {			//sets the boundaries to be # one by one, goes through each array combo. Think of it like for each x check every y  
				if (x == xcan-1 || x == 0) {
					canvas[y][x] = '#';
				}
				if (y == ycan-1 || y == 0) {
					canvas[y][x] = '#';
				}
			}
		}
	}
	
	public String toString() {
		String s = "";
		for (int y = 0; y < canvas.length; y++) {					
			
			for (int x = 0; x < canvas[0].length; x++) {		//actually prints out each character in the array, empty cells included
				s = s+canvas[y][x];				
			}
			s = s+"\n";
		}
		return s;
	}
	
	public void showIt(int x, int y, char c){				//used to test the arena in our main, somewhat obsolete when using drone interface but useful when making changes.
		if (x < xcan-2 && y < ycan-2) {
			canvas[y+1][x+1] = c;
		}
	}
	public static void main(String[] args) {
		ConsoleCanvas c = new ConsoleCanvas (20, 10);	// create a canvas
		c.showIt(4,2,'D');								// add a Drone at 4,2
		System.out.println(c.toString());				// display result
	}


}

