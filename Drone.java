package droneconsole;

public class Drone {

	private int x,y, droneId, dx, dy;
	private Direction di;										// the enum has random direction and next direction clockwise.
	private static int droneCount = 0;


	public Drone (int bx, int by, Direction direction) {		//constructor
		x = bx;
		y = by;
		di = direction;
		droneId = droneCount++;     
	}

	public int getX() {	
		return x;
	}

	public int getY() {
		return y;
	}

	public void setXY(int nx, int ny) {							//ended up not really using this but kept it since in a future design or change it could be utilised.
		x = nx;
		y = ny;
	}

	public boolean isHere (int sx, int sy) {					//A method used in DroneArena powers the logic to check where a drone is.
		if( sx == x && sy == y){
			return true;
		}
		else {
			return false;
		}
	}

	public void tryToMove(DroneArena a) {
		switch(this.di) {
		case North:
			dy = y-1;
			dx = x;

			break;
		case East:    
			dx = x+1;
			dy = y;

			break;
		case South:              								// Remember the 2Darray is drawn top down so the Y axis is flipped.   
			dy = y+1;
			dx = x;

			break;
		case West:
			dx = x-1;
			dy = y;

			break;
		}
		if (a.canMoveHere(dx, dy)) {                            //sends new potential x,y to arena method, if that position is valid it sets that as the new x,y                        
			x = dx;
			y = dy;

		}
		else {
			di = di.next();										//if position is invalid the drone changes direction. This leads to them on the edge of the arena, can be changed to spice things up
		}
		System.out.println(x + " " + y + " " + di);				//mostly here to test but I like how it looks so keep it.
	}


	public String toString() {
		return "Drone " + droneId + " at " + x + ", " + y + " Direction " + di +"\n";
	}

	public void displayDrone(ConsoleCanvas c) {				   // have this last so that postion is correct. Sends postition to the canvas
		c.showIt(x, y, 'D');
	}

	public static void main(String[] args) {
		Drone d = new Drone(9, 5, Direction.getRandomDirection());		// TEST 
		DroneArena a = new DroneArena(10, 5);
		d.tryToMove(a);
	}


}
