package droneconsole;

import java.util.ArrayList;
import java.util.Random;

public class DroneArena {
	
	private Random randomGenerator;
	private ArrayList<Drone>manyDrones;
	
	private int xmax;															//dronesize
	private int ymax;
	
	public DroneArena (int bx, int by) {										//constructor
		xmax = bx;
		ymax = by;
		randomGenerator = new Random();
		manyDrones = new ArrayList<Drone>();
	}
	
	public int getX() {	
		return xmax;
	}
	
	public int getY() {
		return ymax;
	}
	
	public String toString() {
		String s = "";
		for(Drone d : manyDrones) s = s + d.toString();							//uses the drone toString with the enhanced for loop to make things simpler. by the time we are on interface all toStrings can be called with one method 
		return "Arena size " + xmax + " by " + ymax + " with" + "\n"  + s; 
	}
	
	public Drone getDroneAt(int x, int y) {
		
		for(Drone d : manyDrones) {
			
			if(d.isHere(x, y)) {										     	//if no drones are there it returns null, if a drone is there it returns the drone.
				return d;
			}									
		}					
		 return null;
	}
	
	public void addDrone(){
		
		int x = randomGenerator.nextInt(xmax);	
		int y = randomGenerator.nextInt(ymax);
		
		if(this.getDroneAt(x, y) == null) {
			manyDrones.add(new Drone (x,y, Direction.getRandomDirection()));	// checks getDrone at and makes a new drone if there is no drone already in that position. Recursivley loops if there is
		}
		else {
			this.addDrone();
		}
	}
	
	public boolean canMoveHere(int dx, int dy) {								//checks if the drones next position is valid or not, checks walls and other drones. Returns true if move is valid
		
		if (dx > xmax-1 || dx < 0) {
			return false;
		}
		if (dy > ymax-1 || dy < 0) {
			return false;
		}
		if (this.getDroneAt(dx, dy) != null ) {
			return false;
		}
		return true;
	}
	
	public void showDrones(ConsoleCanvas c) {									//Executes the display function on each drone in the array 
		for(Drone d : manyDrones) {
			d.displayDrone(c);
		}
	}
	
	public void moveArena() {													//Executes the move function on each drone om the array
		for(Drone d : manyDrones) {
			d.tryToMove(this);
		}
	}
	
	public static void main(String[] args) {
		DroneArena a = new DroneArena(20, 10);	// TESTING use only 
		a.addDrone();
		a.addDrone();
		a.addDrone();
		System.out.println(a.toString());	
	}


}
